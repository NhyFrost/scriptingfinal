﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CompileTest : MonoBehaviour 
{
	public Button compileButton;
	public InputField inputField;
    public Text inputText;
    public Image img;
    public List<Color> colors;

    Compiler compiler = new Compiler();	
	Script script;
    
	void Awake () 
	{
		compileButton.onClick.AddListener(OnClick);	
	}

	void OnClick()
	{
		if (compiler.Compile(inputField.text))
		{
			MemoryStream ms = ByteCode.SaveToMemory(compiler.GetTables());
			
			ScriptContext context;

			if (ByteCode.Load(ms, out context))
			{
				script = new Script(context);
				script.RegisterFunc(Log);
                script.RegisterFunc(ChangeTheme);
				script.Start();
			}
		}
	}

    public void Log(Value[] values)
    {
        for (int i = 0; i < values.Length; i++)
        {
            switch (values[i].Type)
            {
                case OpType.Int:
                    Debug.Log(values[i].IntLiteral);
                    break;
                case OpType.Float:
                    Debug.Log(values[i].FloatLiteral);
                    break;
                case OpType.String:
                    Debug.Log(values[i].StringLiteral);
                    break;
            }

        }
    }

    public void ChangeTheme(Value[] values)
    {
        if (values[0].Type == OpType.String)
        {
            switch (values[0].StringLiteral)
            {
                case "blue":
                    img.color = colors[0];
                    inputText.color = colors[1];
                    break;
                case "green":
                    img.color = colors[3];
                    inputText.color = colors[2];
                    break;
                case "black":
                    img.color = colors[4];
                    inputText.color = colors[5];
                    break;
                default:
                    Debug.Log("Theme not found");
                    break;
            }
        }
    }

    void Update()
	{
		if (script != null)
			script.RunStep();
	}
}
